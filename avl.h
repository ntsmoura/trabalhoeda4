/*======================= avl.h ===============*/
/*================== Cabeçalho ou header ========*/
#ifndef _H_AVL
#define _H_AVL
	typedef struct no no; //Definição do tipo nó
	typedef struct arvore arvore; //Definição do tipo árvore
	no* criar_no(long int k); //Função para inicializar nó
	no* Avl_busca(arvore* arv, long int k); //Busca elemento na árvore
	long int altura(no* x); //Atualiza altura do nó referente com base na altura das sub-árvores
	no* rotacao_s_esq(arvore* t, no* x); //Rotação simples a esquerda de x
	no* rotacao_s_dir(arvore* t, no* x); //Rotação Simples à direita de x
	no* rotacao_d_esq(arvore* t, no* x); //Rotação dupla à esquerda de x
	no* rotacao_d_dir(arvore* t, no* x); //Rotação dupla à direita de x
	long int balanco(no* x); //Calcula balanceamento do nó e retorna-o
	no* balanceamento(arvore* t, no* x); //Retorna novo nó raiz da sub-arvore após balanceamento ou x caso não tenha sido feito nenhum balanceamento.
	no* Avl_ins(arvore *arv, no* y, no* novo,int op, long int valor); //Insere elemento na AVL de forma recursiva atualiza as alturas e checa balanceamento.
	arvore* Avl_criar(); //Inicializa uma árvore
	no* minimo(no* x); //Minimo da árvore
	no* maximo(no* x); //Maximo da árvore
	no* sucessor(no* x);//Sucessor de um nó (chave)
	no* predecessor(no* x); //predecessor de um nó (chave)
	long int transplante(arvore* t, no* z, no* y); //realiza transplantes com os nós da árvore (1 para procedimento finalizado corretamente)
	no* copia(no* x); //Faz copia do no x
	no* Avl_remover(arvore* t, no* x, long int k); //Remover elemento da Avl
	long int returnCPF(no *x); //Visita nó e retorna chave
	long int returnValor(no *x); //Visita nó e retorna valor
	long int returnOps(no *x); //Visita nó e retorna qtd operações
	no* raiz(arvore* t);//Retorna raiz da árvore
	no* filhoEsquerdo(no* x); //Retorna filho esquerdo de x
	no* filhoDireito(no* x);//Retorna filho direito de x
	long int returnAltura(no* x); //Retorna altura do nó
	long int returnQuantidade(arvore* t); //Quantidade de nós da arvore
#endif
