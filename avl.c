#include <stdlib.h>
#include <stdio.h>
#include "avl.h"
//Definição do nó
struct no{
    long int chave,ops,valor;
    long int altura;
    no* esq;
    no* dir;
    no* pai;
};

//Definiçao da árvore
struct arvore{
    long int altura, qnt;
    no* raiz;
};

//Função para inicializar nó
no* criar_no(long int k){
    no* n = (no*) malloc(sizeof(no));
    if(n == NULL) return NULL;
    else{
        n->chave = k;
        n->esq = NULL;
        n->dir = NULL;
        n->pai = NULL;
        n->valor = 0;
        n->ops = 0;
        n->altura = 0;
        return n;
    }
}

//Busca elemento na árvore
no* Avl_busca(arvore* arv, long int k){
    no* aux = arv->raiz;
    while(aux!=NULL && aux->chave!=k){
        if(k<aux->chave) aux = aux->esq;
        else aux = aux->dir;
    }
    return aux;
}

//Atualiza altura do nó referente com base na altura das sub-árvores
long int altura(no* x){
    if (x->esq != NULL && x->dir != NULL){
        if(x->esq->altura >= x->dir->altura){
            x->altura = x->esq->altura + 1;
        }
        else x->altura = x->dir->altura + 1;
    }
    else if(x->dir == NULL && x->esq == NULL) x->altura = 1;
    else if(x->dir == NULL) x->altura = x->esq->altura + 1;
    else x->altura = x->dir->altura + 1;
    return 1;
}

//Rotação simples a esquerda de x
no* rotacao_s_esq(arvore* t, no* x){
    no* y = x->dir;
    x->dir = y->esq;
    if(x->dir!=NULL) x->dir->pai = x;
    if(t->raiz == x){
        t->raiz = y;
        y->pai = NULL;
    }
    else{
        y->pai = x->pai;
        if(x->pai->esq == x) x->pai->esq = y;
        else x->pai->dir = y;
    }
        y->esq = x;
        x->pai = y;
        altura(x);
        altura(y);
    return y;

}

//Rotação Simples à direita de x
no* rotacao_s_dir(arvore* t, no* x){
    no* y = x->esq;
    x->esq = y->dir;
    if(t->raiz == x){
        t->raiz = y;
        y->pai = NULL;
    }
    else{
        y->pai = x->pai;
        if(x->pai->dir == x) x->pai->dir = y;
        else x->pai->esq = y;
    }
        y->dir = x;
        if(x->esq!=NULL) x->esq->pai = x;
        x->pai = y;
        altura(x);
        altura(y);
    return y;

}

//Rotação dupla à esquerda de x
no* rotacao_d_esq(arvore* t, no* x){
    no* y = x->dir;
    no* z = y->esq;
    x->dir = z->esq;
    if(x->dir != NULL){
        x->dir->pai = x;
    }
    y->esq = z->dir;
    if(y->esq != NULL){
        y->esq->pai = y;
    }
    z->esq = x;
    z->dir = y;
    y->pai = z;
    if(t->raiz == x){
        t->raiz = z;
        z->pai = NULL;
    }
    else{
        z->pai = x->pai;
        if(x->pai->esq==x) x->pai->esq = z;
        else x->pai->dir = z;
    }
    x->pai = z;
    altura(x);
    altura(y);
    altura(z);
    return z;
}

//Rotação dupla à direita de x
no* rotacao_d_dir(arvore* t, no* x){
    no* y = x->esq;
    no* z = y->dir;
    x->esq = z->dir;
    if(x->esq != NULL){
        x->esq->pai = x;
    }
    y->dir = z->esq;
    if(y->dir != NULL){
        y->dir->pai = y;
    }
    z->dir = x;
    z->esq = y;
    y->pai = z;
    if(t->raiz == x){
        t->raiz = z;
        z->pai = NULL;
    }
    else{
        z->pai = x->pai;
        if(x->pai->dir==x) x->pai->dir = z;
        else x->pai->esq = z;
    }
    x->pai = z;
    altura(x);
    altura(y);
    altura(z);
    return z;
}

//Calcula balanceamento do nó e retorna-o
long int balanco(no* x){
	if(x!=NULL){
		if(x->dir != NULL && x->esq!=NULL){
			return x->esq->altura - x->dir->altura;
		}
		else if(x->dir!=NULL){
			return (x->dir->altura)*-1;
		}
		else if(x->esq!=NULL) return (x->esq->altura);
		}
    return 0;
}

//Retorna novo nó raiz da sub-arvore após balanceamento ou x caso não tenha sido feito nenhum balanceamento.
no* balanceamento(arvore* t, no* x){
	if(balanco(x)==-2){
		no* y = x->dir;
		if(balanco(y)==1) return rotacao_d_esq(t,x);
		else return rotacao_s_esq(t,x);
	}
	if(balanco(x)==2){
		no* y = x->esq;
		if(balanco(y)==-1) return rotacao_d_dir(t,x);
		else return rotacao_s_dir(t,x);
	}
    return x;
}


//Insere elemento na AVL de forma recursiva atualiza as alturas e checa balanceamento. 
no* Avl_ins(arvore *arv, no* y, no* novo, int op,long int valor){
    if(arv->raiz == NULL){
        arv->raiz = novo;
        novo->pai = NULL;
        y = arv->raiz;
        novo->ops++;
        if(op==1){
        novo->valor-=valor;
        }
        else novo->valor+=valor;
        arv->qnt++;
        altura(novo);
    }
    else{
        if(y==NULL) y = arv->raiz;
        if(y->chave == novo->chave){
            y->ops++;
            if(op==1){
                y->valor-=valor;
            }
            else y->valor+=valor;
            free(novo); //Libera nó pois já existe um com a chave dele.
        }
        else if(novo->chave < y->chave){
            if(y->esq == NULL){
                y->esq = novo;
                novo->pai = y;
                altura(novo);
                novo->ops++;
                if(op==1){
                novo->valor-=valor;
                }
                else novo->valor+=valor;
                arv->qnt++;
            }
            else{
                Avl_ins(arv,y->esq,novo,op,valor);
            }
        }
        else if(novo->chave > y->chave){
            if(y->dir == NULL){
                y->dir = novo;
                novo->pai = y;
                altura(novo);
                novo->ops++;
                if(op==1){
                novo->valor-=valor;
                }
                else novo->valor+=valor;
                arv->qnt++;
            }
            else{
                Avl_ins(arv,y->dir,novo,op,valor);
            }
        }
    }
    altura(y);
    if(balanco(y)== 2 || balanco(y)==-2) balanceamento(arv,y);
    return y;
}

//Inicializa uma árvore
arvore* Avl_criar(){
    arvore* arv = (arvore*) malloc(sizeof(arvore));
    if(arv==NULL) return NULL;
    else{
        arv->altura=0;
        arv->qnt=0;
        arv->raiz = NULL;
        return arv;
    }
}

//Minimo da árvore
no* minimo(no* x){
	while(x->esq != NULL) x = x->esq;
	return x;
}

//Maximo da árvore
no* maximo(no* x){
	while(x->dir != NULL) x = x->dir;
	return x;
}

//Sucessor de um nó (chave)
no* sucessor(no* x){
	if (x->dir !=NULL){
		return minimo(x->dir);
	}
	no* y = x->pai;
	while(y!=NULL && y->dir==x){
		x = y;
		y = x->pai;
	}
	return y;
}

//predecessor de um nó (chave)
no* predecessor(no* x){
	if (x->esq !=NULL){
		return maximo(x->esq);
	}
	no* y = x->pai;
	while(y!=NULL && y->esq==x){
		x = y;
		y = x->pai;
	}
	return y;
}

//realiza transplantes com os nós da árvore (1 para procedimento finalizado corretamente)
long int transplante(arvore* t, no* z, no* y){
	if(z->pai == NULL){
		t->raiz = y;
		//if(y!=NULL) y->pai = NULL;
	}
	else if(z == z->pai->esq){
		z->pai->esq = y;
	}
	else{
		z->pai->dir = y;
	}
	if(y!=NULL){
		y->pai = z->pai;
	}
    return 1;
}

//Retorna copia do no X
no* copia(no* x){
    no* n = (no*) malloc(sizeof(no));
    if(n == NULL) return NULL;
    else{
        n->chave = x->chave;
        n->esq = x->esq;
        n->dir = x->dir;
        n->pai = x->pai;
        n->altura = x->altura;
        n->ops = x->ops;
        n->valor = x->valor;
        return n;
    }
}

//Remover elemento da Avl
no* Avl_remover(arvore* t, no* x, long int k){
    if(x == NULL) return NULL;
    if(k < x->chave) Avl_remover(t,x->esq,k);
    else if(k > x->chave) Avl_remover(t,x->dir,k);
    else{
        if(x->esq == NULL){
            t->qnt--;
            transplante(t,x,x->dir);
            free(x); //Libera nó que foi removido
            x = NULL;
        }
         else if(x->dir == NULL){
            t->qnt--;
            transplante(t,x,x->esq);
            free(x); //Libera nó que foi removido
            x = NULL;
         }
         else{
            no* y = minimo(x->dir);
            no* z = copia(y);
            transplante(t,x,z);
            z->esq = x->esq;
            z->dir = x->dir;
            z->dir->pai = z;
            z->esq->pai = z;
            free(x); //Libera o nó para qual x apontava, isso garante não haver memory leak
            x=NULL; //Associa NULL ao ponteiro 
            x = z; //Associa novo valor para x
            Avl_remover(t,x->dir,y->chave);
        }
    }
    if(x == NULL) return NULL;
    altura(x);
    if(balanco(x)== 2 || balanco(x)==-2) balanceamento(t,x);
    return x;
}

//Visita nó e retorna chave
long int returnCPF(no *x){
    return x->chave;
}

//Visita nó e retorna valor
long int returnValor(no *x){
    return x->valor;
}

//Visita nó e retorna qtd operações
long int returnOps(no *x){
    return x->ops;
}

//Retorna raiz de t
no* raiz(arvore* t){
    return t->raiz;
}

//Retorna filho esquerdo do nó
no* filhoEsquerdo(no* x){
    return x->esq;
}

//Retorna filho direito do nó
no* filhoDireito(no* x){
    return x->dir;
}

//Retorna altura do nó
long int returnAltura(no* x){
    return x->altura;
}
//Retorna quantidade de nós da árvore
long int returnQuantidade(arvore* t){
    return t->qnt;
}