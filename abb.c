#include<stdlib.h>
#include<stdio.h>

typedef struct no no;
typedef struct arvore arvore;

//Definição do nó
struct no{
    long int chave;
    long int altura;
    no* esq;
    no* dir;
    no* pai;
};

//Definiçao da árvore
struct arvore{
    long int altura, qnt;
    no* raiz;
};

//Função para inicializar nó
no* criar_no(long int k){
    no* n = (no*) malloc(sizeof(no));
    n->chave = k;
    n->esq = NULL;
    n->dir = NULL;
    n->pai = NULL;
    n->altura = 0;
    return n;
}

//Busca elemento na árvore
no* Abb_busca(arvore* arv, long int k){
    no* aux = arv->raiz;
    while(aux!=NULL && aux->chave!=k){
        if(k<aux->chave) aux = aux->esq;
        else aux = aux->dir;
    }
    return aux;
}

long int altura(no* x){
    if (x->esq != NULL && x->dir != NULL){
        if(x->esq->altura >= x->dir->altura){
            x->altura = x->esq->altura + 1;
        }
        else x->altura = x->dir->altura + 1;
    }
    else if(x->dir == NULL && x->esq == NULL) x->altura = 1;
    else if(x->dir == NULL) x->altura = x->esq->altura + 1;
    else x->altura = x->dir->altura + 1;
}

/*Insere elemento na árvore iterativamente
long int Avl_inserir(arvore *arv, no* novo){
    no* y = NULL;
    no* x = arv->raiz;
    while(x!=NULL){
        y = x;
        if(novo->chave < x->chave)
            x = x->esq;
        else if(novo->chave > x->chave)
            x = x->dir;
        else
            return 0;
    }
    novo->pai = y;
    if(y==NULL){
        arv->raiz = novo;
    } else if(novo->chave < y->chave)
        y->esq = novo;
    else
        y->dir = novo;
    arv->qnt++;
    return 1;
}*/

//Rotação simples a esquerda de x
no* rotacao_s_esq(arvore* t, no* x){
    no* y = x->dir;
    x->dir = y->esq;
    if(x->dir!=NULL) x->dir->pai = x;
    if(t->raiz == x){
        t->raiz = y;
        y->pai = NULL;
    }
    else{
        y->pai = x->pai;
        if(x->pai->esq == x) x->pai->esq = y;
        else x->pai->dir = y;
    }
        y->esq = x;
        x->pai = y;
        altura(x);
        altura(y);
    return y;

}

//Rotação Simples à direita de x
no* rotacao_s_dir(arvore* t, no* x){
    no* y = x->esq;
    x->esq = y->dir;
    if(t->raiz == x){
        t->raiz = y;
        y->pai = NULL;
    }
    else{
        y->pai = x->pai;
        if(x->pai->dir == x) x->pai->dir = y;
        else x->pai->esq = y;
    }
        y->dir = x;
        if(x->esq!=NULL) x->esq->pai = x;
        x->pai = y;
        altura(x);
        altura(y);
    return y;

}

//Rotação dupla à esquerda de x
no* rotacao_d_esq(arvore* t, no* x){
    no* y = x->dir;
    no* z = y->esq;
    x->dir = z->esq;
    if(x->dir != NULL){
        x->dir->pai = x;
    }
    y->esq = z->dir;
    if(y->esq != NULL){
        y->esq->pai = y;
    }
    z->esq = x;
    z->dir = y;
    y->pai = z;
    if(t->raiz == x){
        t->raiz = z;
        z->pai = NULL;
    }
    else{
        z->pai = x->pai;
        if(x->pai->esq==x) x->pai->esq = z;
        else x->pai->dir = z;
    }
    x->pai = z;
    altura(x);
    altura(y);
    altura(z);
    return z;
}

//Rotação dupla à direita de x
no* rotacao_d_dir(arvore* t, no* x){
    no* y = x->esq;
    no* z = y->dir;
    x->esq = z->dir;
    if(x->esq != NULL){
        x->esq->pai = x;
    }
    y->dir = z->esq;
    if(y->dir != NULL){
        y->dir->pai = y;
    }
    z->dir = x;
    z->esq = y;
    y->pai = z;
    if(t->raiz == x){
        t->raiz = z;
        z->pai = NULL;
    }
    else{
        z->pai = x->pai;
        if(x->pai->dir==x) x->pai->dir = z;
        else x->pai->esq = z;
    }
    x->pai = z;
    altura(x);
    altura(y);
    altura(z);
    return z;
}

long int balanco(no* x){
	if(x!=NULL){
		if(x->dir != NULL && x->esq!=NULL){
			return x->esq->altura - x->dir->altura;
		}
		else if(x->dir!=NULL){
			return (x->dir->altura)*-1;
		}
		else if(x->esq!=NULL) return (x->esq->altura);
		}
	else return 0;
}

void balanceamento(arvore* t, no* x){
	//printf("\nBALAN X:%d ", balanco(x));
	if(balanco(x)==-2){
		no* y = x->dir;
		//printf("\nBALAN Y:%d ", balanco(y));
		if(balanco(y)==1) rotacao_d_esq(t,x);
		else rotacao_s_esq(t,x);
	}
	if(balanco(x)==2){
		no* y = x->esq;
		//printf("\nBALAN Y:%d ", balanco(y));
		if(balanco(y)==-1) rotacao_d_dir(t,x);
		else rotacao_s_dir(t,x);
	}
}


//Insere elemento na AVL de forma recursiva atualiza as alturas e checa balanceamento.
no* Avl_ins(arvore *arv, no* y, no* novo){
    if(arv->raiz == NULL){
        arv->raiz = novo;
        novo->pai = NULL;
        y = arv->raiz;
        arv->qnt++;
        altura(novo);
    }
    else{
        if(y==NULL) y = arv->raiz;
        if(novo->chave < y->chave){
            if(y->esq == NULL){
                y->esq = novo;
                novo->pai = y;
                altura(novo);
                arv->qnt++;
            }
            else{
                Avl_ins(arv,y->esq,novo);
            }
        }
        else if(novo->chave > y->chave){
            if(y->dir == NULL){
                y->dir = novo;
                novo->pai = y;
                altura(novo);
                arv->qnt++;
            }
            else{
                Avl_ins(arv,y->dir,novo);
            }
        }
    }
    altura(y);
    if(balanco(y)== 2 || balanco(y)==-2) balanceamento(arv,y);
    return y;
}

//Inicializa uma árvore
arvore* Abb_criar(){
    arvore* arv = (arvore*) malloc(sizeof(arvore));
    arv->altura=0;
    arv->qnt=0;
    arv->raiz = NULL;
    return arv;
}

//Exibição pre-ordem
no* pre_ordem(no* x){
    if(x!=NULL){
        printf("%d ",x->chave);
        pre_ordem(x->esq);
        pre_ordem(x->dir);
    }
}

//Exibição in-ordem
no* in_ordem(no* x){
    if(x!=NULL){
        in_ordem(x->esq);
        printf("%d ",x->chave);
        in_ordem(x->dir);
    }
}


//Exibição pre-ordem
no* pos_ordem(no* x){
    if(x!=NULL){
        pos_ordem(x->esq);
        pos_ordem(x->dir);
        printf("%d ",x->chave);
    }
}

//Minimo da árvore
no* minimo(no* x){
	while(x->esq != NULL) x = x->esq;
	return x;
}

//Minimo da árvore
no* maximo(no* x){
	while(x->dir != NULL) x = x->dir;
	return x;
}

//Sucessor de um nó (chave)
no* sucessor(no* x){
	if (x->dir !=NULL){
		return minimo(x->dir);
	}
	no* y = x->pai;
	while(y!=NULL && y->dir==x){
		x = y;
		y = x->pai;
	}
	return y;
}

//predecessor de um nó (chave)
no* predecessor(no* x){
	if (x->esq !=NULL){
		return maximo(x->esq);
	}
	no* y = x->pai;
	while(y!=NULL && y->esq==x){
		x = y;
		y = x->pai;
	}
	return y;
}

//realiza transplantes com os nós da árvore
void transplante(arvore* t, no* z, no* y){
	if(z->pai == NULL){
		t->raiz = y;
		//if(y!=NULL) y->pai = NULL;
	}
	else if(z == z->pai->esq){
		z->pai->esq = y;
	}
	else{
		z->pai->dir = y;
	}
	if(y!=NULL){
		y->pai = z->pai;
	}
}

//Remover elemento da ABB
no* Abb_remover(arvore* t, no* z){
	if(z->esq == NULL) transplante(t,z,z->dir);
	else if(z->dir==NULL) transplante(t,z,z->esq);
	else{
		no* y=minimo(z->dir);
		if(y->pai!=z){
			transplante(t,y,y->dir);
			y->dir = z->dir;
			y->dir->pai = y;
		}	
		transplante(t,z,y);
		y->esq = z->esq;
		y->esq->pai = y;
	}
	return z;

}

long int main(){
    arvore* arv = Abb_criar();
    long int n;
    while(1==1){
        scanf("%d",&n);
        if(n==-1) break;
        else{
            if(Avl_ins(arv,NULL,criar_no(n))==0) printf("Elemento repetido!\n");
            
        }
    }

    /*printf("%p",Abb_busca(arv,10));

    no* n4 = criar_no(5);
    printf("%d",Avl_inserir(arv,n4));*/

    //pre_ordem(arv->raiz);
    //printf("\n");
    pre_ordem(arv->raiz);
    //printf("\n");
    //printf("QNT: %d\n",arv->qnt);
    //pos_ordem(arv->raiz);

    
   /* Remover  
   while(1==1){
        scanf("%d",&n);
        if(n==-1) break;
        else{
        	no* busca = Abb_busca(arv,n);
        	if(busca==NULL)printf("Não encontrado!\n");
    		else Abb_remover(arv,busca);
        }
        in_ordem(arv->raiz);
        printf("\n");
    }*/
}