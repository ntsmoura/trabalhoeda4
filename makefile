############################# Makefile ##########################
all: banco
banco: avl.o main.o
		gcc -o banco avl.o main.o
avl.o: avl.c
		gcc -o avl.o -c avl.c -Wall -Wextra -Werror -Wpedantic
main.o: main.c avl.h
		gcc -o main.o -c main.c -Wall -Wextra -Werror -Wpedantic
clean:
		rm -rf *.o
mrproper: clean
