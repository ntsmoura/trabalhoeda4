//Natan Moura
#include<stdlib.h>
#include<stdio.h>
#include "avl.h"
//Exibição crescente, ESQ - RAIZ - DIR (operação "p")
int in_ordemc(no* x){
    if(x!=NULL){
        in_ordemc(filhoEsquerdo(x));
        printf("%ld %ld %ld\n",returnCPF(x), returnOps(x),returnValor(x));
        in_ordemc(filhoDireito(x));
    }
    return 1;
}
//Exibição decrescente, DIR - RAIZ - ESQ (operação "p")
int in_ordemd(no* x){
    if(x!=NULL){
        in_ordemd(filhoDireito(x));
        printf("%ld %ld %ld\n",returnCPF(x), returnOps(x),returnValor(x));
        in_ordemd(filhoEsquerdo(x));
    }
    return 1;
}
//Exibição por nível, exibe itens quando o nível é alcançado (operação "n")
int in_ordem_level(no*x, long int cont,long int n){
    if(x!=NULL){
        if(cont == n){
            printf("%ld\n",returnCPF(x));
        }else{
            in_ordem_level(filhoEsquerdo(x),cont+1,n);
            in_ordem_level(filhoDireito(x),cont+1,n);
        }
    }
    return 1;
}
//Prepara e insere nós na árvore (operação "i")
int insere_no(arvore* t, long int chave, int operacao, long int valor){
    no* x = criar_no(chave); //Talvez dê memory leak aqui
    Avl_ins(t,NULL,x,operacao,valor);
    return 1;
}
//Constula existência do nó na árvore (operação "c")
int consulta_no(arvore* t, long int chave){
    no* k = Avl_busca(t,chave);
    if(k == NULL) printf("nao existe no com chave: %ld\n",chave);
    else  printf("existe no com chave: %ld\n",chave);
    return 1;
}
//Remove nó da árvore com chave determinada (se existir), (operação "r")
int remover_no(arvore* t,long int chave){
    Avl_remover(t,raiz(t),chave);
    return 1;
}
//Controla o tipo de exibição, se "crescente" ou "decrescente", responsável por gerir as opções da operação "p"
int listar_no(arvore* t,char ordem){
    if(ordem =='c') in_ordemc(raiz(t));
    else if(ordem =='d') in_ordemd(raiz(t));
    return 1;
}
//Informa altura da árvore (operação "h")
int informa_altura(arvore* t){
    if(raiz(t)==NULL) printf("0\n");
    else printf("%ld\n",returnAltura(raiz(t)));
    return 1;
}
//Chama a função para exibição de nós do nível desejado, operação "n"
int listar_nivel(arvore* t,long int nivel){
    in_ordem_level(raiz(t),1,nivel);
    return 1;
}
//Relatório final, chamado após finalização do atendimento com a operação "f" (ordem de exibição dada pela remoção da raiz)
int relatorio_final(arvore* t){
    printf("-+- Inicio relatorio -+-\n");
    printf("%ld\n",returnQuantidade(t));
    while(raiz(t)!=NULL){
        no* x = raiz(t);
        printf("%ld %ld %ld\n",returnCPF(x), returnOps(x),returnValor(x)); //Imprime elementos da raiz
        Avl_remover(t,raiz(t),returnCPF(raiz(t))); //Remove a raiz
    }
    printf("-+- Fim relatorio -+-\n");
    return 1;
}
int main(){
arvore *t = Avl_criar();
   char op = ' ',ordem;
   long int chave,valor;
   int operacao;
    while(op!='f'){
    scanf("%c",&op);
        switch (op)
        {
            case 'i':{
                scanf("%ld%d%ld",&chave,&operacao,&valor);
                insere_no(t,chave,operacao,valor);
            }
            break;
            case'c':{
                scanf("%ld",&chave);
                consulta_no(t,chave);
            }
            break;
            case'r':{
                scanf("%ld",&chave);
                remover_no(t,chave);
            }
            break;
            case 'p':{
                scanf(" %c",&ordem);
                listar_no(t,ordem);
            }
            break;
            case 'h':{
                informa_altura(t);
            }
            break;
            case 'n':{
                long int n;
                scanf("%ld",&n);
                listar_nivel(t,n);
            }
        }
    }
    relatorio_final(t);
    free(t);
}
